# Normal Multivariada

Cria gráficos em python da distribuição normal multivariada.

A densidade da normal multivariada é na forma

```math
    f(\bm x, \bm \mu, \bm \Sigma) =
    \frac{1}{\sqrt{(2 \pi) ^ n \det (\bm \Sigma)}}
    \exp \left(
        -\frac{1}{2}
        (\bm{x - \mu}) ^ T
        \bm \Sigma ^{-1}
        (\bm{x - \mu})
    \right),
```
onde $\bm x$ e $\bm \mu$ é um vetor de dimensão $n$, e $\bm \Sigma$ é uma matriz de dimensão $n \times n$.

O resultado resultado final se encontra abaixo.
![](normal_multivariada.png)
