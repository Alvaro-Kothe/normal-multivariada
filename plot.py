import matplotlib.pyplot as plt
import numpy as np


def multivariate_gaussian(x: np.ndarray, mu: np.ndarray, sigma: np.ndarray):
    """Density of multivariate gaussian"""
    n = mu.shape[0]

    denominator = 1 / np.sqrt(((2 * np.pi) ** n) * np.linalg.det(sigma))
    sigma_inv = np.linalg.inv(sigma)

    exp_term = np.exp(-np.einsum("ijk,kl,ijl->ij", x - mu, sigma_inv, x - mu) / 2)
    density = exp_term / denominator

    return density


if __name__ == "__main__":
    N = 100
    x = np.linspace(-3, 3, N)
    y = np.linspace(-3, 3, N)
    xx, yy = np.meshgrid(x, y)
    x_y = np.stack((xx, yy), axis=-1)

    mu = np.array([0.0, 0.5])
    sigma = np.array([[1, 0.7], [0.7, 1]])

    zs = multivariate_gaussian(x_y, mu, sigma)

    fig = plt.figure()
    ax = plt.axes(projection="3d")
    ax.plot_surface(
        xx,
        yy,
        zs,
        rstride=3,
        cstride=3,
        linewidth=1,
        antialiased=True,
        cmap="viridis",
        edgecolor="none",
    )
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.savefig("normal_multivariada.png")
